function createList(arr, parent = document.body) {
  const ul = document.createElement("ul");

  arr.forEach((item) => {
    const li = document.createElement("li");
    if (Array.isArray(item)) {
      li.appendChild(createList(item));
    } else {
      li.textContent = item;
    }
    ul.appendChild(li);
  });

  parent.appendChild(ul);
}
const data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createList(data);
let secondsLeft = 3;
const countdown = document.createElement("div");
countdown.textContent = `Очищення через ${secondsLeft} секунд`;
document.body.appendChild(countdown);

function updateCountdown() {
  secondsLeft--;
  countdown.textContent = `Очищення через ${secondsLeft} секунд`;
  if (secondsLeft === 0) {
    document.body.innerHTML = "";
  } else {
    setTimeout(updateCountdown, 1000);
  }
}

setTimeout(updateCountdown, 1000);
